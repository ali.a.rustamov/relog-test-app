import { Client } from './types/Client';

export const fetchNeRelogApps = () =>
  fetch('/data/input_data_5000/NeRelog_apps.json')
    .then(response => response.json());

export const fetchNeRelogClients = (): Promise<Client[]> =>
  fetch('/data/input_data_5000/NeRelog_clients.json')
    .then(response => response.json());
