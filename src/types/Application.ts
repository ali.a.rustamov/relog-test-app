import { DeliveryType } from './DeliveryType';

export type Application = {
  id: number;
  type: DeliveryType;
  price: number;
  coords: {
    lat: number;
    long: number;
  },
  client_id: number;
}