import React, { useEffect, useState } from 'react';
import './App.css';
import { fetchNeRelogApps, fetchNeRelogClients } from './api';
import { ApplicationMap } from './components/ApplicationMap';
import { Application } from './types/Application';
import { Client } from './types/Client';
import 'leaflet/dist/leaflet.css';

function App() {

  const [apps, setApps] = useState<ReadonlyArray<Application> | undefined>();
  const [clients, setClients] = useState<ReadonlyArray<Client> | undefined>();
  const [clientsMap, setClientsMap] = useState<Record<number, Client>>();

  useEffect(() => {
    fetchNeRelogApps().then(json => {
      setApps(json);
    });
  }, []);

  useEffect(() => {
    // apps && console.log(apps);
  }, [apps])

  useEffect(() => {
    fetchNeRelogClients().then(json => {
      setClients(json);
      setClientsMap(json.reduce((prev, curr) => ({
        ...prev,
        [curr.id]: curr,
      }) ,{}));
    });
  }, []);

  useEffect(() => {
    // clients && console.log(clients);
  }, [clients]);

  useEffect(() => {
    // clientsMap && console.log(clientsMap);
  }, [clientsMap]);

  return (
    <div className="App">
      {clientsMap && <ApplicationMap apps={apps} clientsMap={clientsMap} />}
    </div>
  );
}

export default App;
