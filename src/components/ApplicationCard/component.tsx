import { FC } from 'react';
import { ApplicationCardProps } from './props';
import { DeliveryType } from '../../types/DeliveryType';

export const ApplicationCard: FC<ApplicationCardProps> = ({app, client, onAnchorClick, ...rest}) => {

  return <a href="#" onClick={onAnchorClick(app)}>
    <div {...rest} className="w-full border-l border-b hover:bg-lightblue text-left pl-4">
      Клиент: {client.name}
      <br />
      Тип: {app.type === DeliveryType.Delivery ? 'Доставка' : 'Самовывоз'}
      <br />
      Цена: {app.price}
    </div>
  </a>;
};