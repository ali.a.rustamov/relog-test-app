import { HTMLAttributes, MouseEventHandler } from 'react';
import { Application } from '../../types/Application';
import { Client } from '../../types/Client';

export type ApplicationCardProps = HTMLAttributes<HTMLDivElement> & {
  app: Application;
  client: Client;
  onAnchorClick: (app: Application) => MouseEventHandler<HTMLAnchorElement>;
};