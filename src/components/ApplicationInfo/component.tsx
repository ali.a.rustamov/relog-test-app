import { FC } from 'react';
import { ApplicationInfoProps } from './props';

export const ApplicationInfo: FC<ApplicationInfoProps> = ({application, client, ...rest}) => <div {...rest}>
  <p>ID заказа: {application.id}</p>
  <p>Клиент: {client.name}</p>
  <p>Цена: {application.price}</p>
</div>;