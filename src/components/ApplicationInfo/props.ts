import { HTMLAttributes } from 'react';
import { Application } from '../../types/Application';
import { Client } from '../../types/Client';

export type ApplicationInfoProps = HTMLAttributes<HTMLDivElement> & {
  application: Application;
  client: Client;
};