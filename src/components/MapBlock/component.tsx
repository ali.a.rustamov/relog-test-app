import { MapContainer, MapContainerProps, TileLayer } from 'react-leaflet';
import { FC } from 'react';

export const MapBlock: FC<MapContainerProps> = ({ style, center= [43.238949, 76.889709], children, ...rest}) => {
  return <MapContainer className="h-150" center={center} zoom={15} scrollWheelZoom {...rest}>
    <TileLayer
      attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
    {children}
  </MapContainer>;
}