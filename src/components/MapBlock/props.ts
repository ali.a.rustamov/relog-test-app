import { HTMLAttributes } from 'react';

export type MapBlockProps = HTMLAttributes<HTMLDivElement>;