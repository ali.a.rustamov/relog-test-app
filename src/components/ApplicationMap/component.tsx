import { FC, MouseEventHandler, useCallback } from 'react';
import { ApplicationMapProps } from './props';
import { Application } from '../../types/Application';
import { ApplicationCard } from '../ApplicationCard';
import { FixedSizeList as List } from 'react-window';
import { MapBlock } from '../MapBlock';
import { MapMarker } from '../MapMarker';
import MarkerClusterGroup from 'react-leaflet-cluster';
import { ApplicationInfo } from '../ApplicationInfo';

export const ApplicationMap: FC<ApplicationMapProps> = ({
    apps,
    clientsMap,
    ...rest}) =>
  {
    const handleAppClick = useCallback<(app: Application) => MouseEventHandler<HTMLAnchorElement>>((app) => (e) => {
      console.log(app.coords.lat, app.coords.long);
    }, []);

    return (
      <div className="flex w-full flex-wrap" {...rest}>
        {apps &&
          (
            <div className="lg:w-1/4 md:w-1/2 w-full">
              <List height={600} width="100%" itemCount={apps?.length} itemSize={73}
                    className="List">
                {({index, style}) =>
                  <ApplicationCard
                    app={apps[index]}
                    client={clientsMap?.[apps[index].client_id]}
                    onAnchorClick={handleAppClick} style={style}/>
                }
              </List>
            </div>
          )
        }
        {<div className="lg:w-3/4 md:w-1/2 w-full border border-l-0">
            <MapBlock>
                <MarkerClusterGroup chunkedLoading>
                  {apps?.map(app => <MapMarker position={[app.coords.lat, app.coords.long]} >
                    <ApplicationInfo application={app} client={clientsMap?.[app.client_id]} />
                  </MapMarker>)}
                </MarkerClusterGroup>
            </MapBlock>
        </div>}

      </div>
    );
};