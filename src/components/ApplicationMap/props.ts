import { HTMLAttributes } from 'react';
import { Application } from '../../types/Application';
import { Client } from '../../types/Client';

export type ApplicationMapProps = HTMLAttributes<HTMLDivElement> & {
  apps?: ReadonlyArray<Application>;
  clientsMap: Record<number, Client>;
};