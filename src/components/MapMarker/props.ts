import { MarkerProps } from 'react-leaflet';
import { LatLngExpression } from 'leaflet';

export type MapMarkerProps = Omit<MarkerProps, 'position'> & {
  position?: LatLngExpression;
};