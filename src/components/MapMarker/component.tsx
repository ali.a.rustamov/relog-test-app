import { FC } from 'react';
import { MapMarkerProps } from './props';
import { Marker, Popup } from 'react-leaflet';
import { icon } from 'leaflet';
import iconUrl from 'leaflet/dist/images/marker-icon-2x.png'

export const MapMarker: FC<MapMarkerProps> = ({ position= [43.238949, 76.889709], children }) =>
  <Marker
    position={position}
    icon={icon({ iconUrl, iconAnchor: [25, 82], })}
    eventHandlers={{
        mouseover: (e) => {
            e.target.openPopup();
        },
    }}
  >
    <Popup>
      {children}
    </Popup>
</Marker>;